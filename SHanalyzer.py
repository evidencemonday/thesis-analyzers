from elftools.elf.elffile import *
from elftools.common.exceptions import ELFError
import re
import magic
from pwn import *
import subprocess
import re, os


class Shell:
    def __init__(self, path):
        self.name = "Checking for 'password-like' string in {}...".format(path)
        self.path = path

    def check_file(self):
        filetype = magic.from_file(path, mime=True)
        if filetype  == "text/x-shellscript" or filetype== "text/x-python" or filetype=="text/plain":
            print(self.name)
            return True
        else:
            print(self.name)
            return False

    

    def code_function(self):
        added = []
        pattern = "^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$"

        # Initialize input file processing
        out= subprocess.check_output("strings {}".format(path), shell=True)
        path_list = out.decode("utf-8").splitlines()
        
        
        # Initialize mirai.txt processing
        mi_path = './mirai.txt'
        out_mi= subprocess.check_output("strings {}".format(mi_path), shell=True)
        mi_list = out_mi.decode("utf-8").splitlines()

        # Initialize pastebin.txt processing
        pastebin_path = './pastebin.txt'
        out_paste= subprocess.check_output("strings {}".format(pastebin_path), shell=True)
        paste_list = out_paste.decode("utf-8").splitlines()

        keywords = ['pass', 'system', 'false', 'await', 'else','import', 'pass', 'None', 'break', 'except', 'in', 'raise', 'true', 'class',	'finally', 'is', 'return', 'and', 'continue', 'for', 'lambda', 'try', 'as', 'def', 'from', 'nonlocal', 'while', 'assert', 'del', 'global','not', 'with', 'async', 'elif', 'if', 'or', 'yield']


        # Step 1: Loops through a list of password & username used in Mirai botnet attack
        for line in path_list:
            for s in mi_list:
                if re.search(s, line, re.I):
                    # Confirm if 's' is a keyword
                    if s.lower() in keywords:
                        if 'password' not in line.lower():
                            pass
                        elif line.lower() in added:
                            break
                        else:
                            added.append(line)
                            break
                    elif line.lower() in added:
                        break
                    else:
                        added.append(line)
                        break
                else:
                    pass

        # Step 2: Loops through a list of compiled password & username used in pastebin
        for line in path_list:
            for s in paste_list:
                if re.search(s, line, re.I):
                    # Confirm if 's' is a keyword
                    if s.lower() in keywords:
                        if 'password' not in line.lower():
                            pass
                        elif line.lower() in added:
                            break
                        else:
                            added.append(line)
                            break

                    elif line.lower() in added:
                        break
                    else:
                        added.append(line)
                        break
                else:
                    pass

        # Step 3: Search for password patterns in file
        for line in path_list:
            if re.search(pattern, line, re.I):
                if line.lower() in added:
                    break
                else:
                    added.append(line)
                    break
            else:
                pass

        if len(added) > 0:
            print(added)
        else:
            print('No password-like pattern found in {}'.format(path))

    def is_shell(self):
        status = False
        if os.path.exists(path):
            try:
                if self.check_file() == True:
                    status = True
                    print("FileType status: \033[92m{}\033[39m".format(status))
                    self.code_function()
                else:
                    status = False
                    print("FileType Status:  \033[91m{}\033[39m".format(status))
                    print("FileType Error. {} is not shell script/ python/ text file".format(path))
            except IOError:
                print("cannot open {}".format(path))
        else:
            print("No such directory: {}".format(path))
            sys.exit

    

# path ='./test_files/test.sh'
# analyzer = Shell(path)
# analyzer.is_shell()
