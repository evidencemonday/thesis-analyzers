import os
import sys
import lief
import math
import magic
import hashlib
import subprocess
from elftools.elf.elffile import *
from elftools.common.exceptions import ELFError
import re
from pwn import *
from fortify_check import Fortify


class ELF_Functions:
    def __init__(self, path):
        self.name = "Dangerous functions used are: "
        self.dstrings = "Interesting strings found are: "
        self.path = path

    def main(self):
        try: 
            self.is_elf()        
            self.entropy()
            self.checksum()
            self.dangerous_funcs()
            self.check_sec()
            self.get_packed()
            self.reference_func()
            self.dependency_check()
        except TypeError:
            return "Type Error"
        

    def is_elf(self):
        status = True
        if os.path.exists(path):
            try:
                with open(path, 'rb') as f:
                    try:
                        elf = ELFFile(f)
                        machine_type = elf["e_machine"]
                        if "EM_NONE" in machine_type:
                            machine_type = "No machine"
                        elif "M32" in machine_type:
                            machine_type = "AT&T WE 32100"
                        elif "EM_SPARC" in machine_type:
                            machine_type = "SPARC"
                        elif "EM_386" in machine_type:
                            machine_type = "Intel 80386"
                        elif "EM_68K" in machine_type:
                            machine_type = "Motorola 68000"
                        elif "EM_88K" in machine_type:
                            machine_type = "Motorola 88000"
                        elif "EM_860" in machine_type:
                            machine_type = "Intel 80860"
                        elif "EM_MIPS" in machine_type:
                            machine_type = "MIPS I Architecture"
                        elif "EM_S370" in machine_type:
                            machine_type = "IBM System/370 Processor"
                        elif "EM_PARISC" in machine_type:
                            machine_type = "Hewlett-Packard PA-RISC"
                        elif "EM_NCPU" in machine_type:
                            machine_type = "Sony nCPU"
                        else:
                            machine_type = "Others"

                        ident = elf["e_ident"]
                        ident_list = list(ident.values())[0]
                        
                        
                        typef = elf["e_type"]
                        if "ET_DYN" in typef:
                            typef = "DYNAMIC, Shared Object file"
                        elif "ET_none" in typef:
                            typef = "No file type"
                        elif "ET_REL" in typef:
                            typef = "Relocatable file"
                        elif "ET_EXEC" in typef:
                            typef = "Executable file"
                        elif "ET_CORE" in typef:
                            typef = "Core file"
                        elif "ET_NUM" in typef:
                            typef = "Five file types"
                        elif "ET_LOPROC" or "ET_HIPROC" in typef:
                            typef = "Processor-specific"
                        else:
                            typef = "Unknown file type"


                        e_entry = elf["e_entry"]

                        vers = elf["e_version"]
                        if "EV_NONE" in vers:
                            vers = "Invalid version"
                        elif "EV_CURRENT" in vers:
                            vers = "CURRENT"
                        elif "EV_NUM" in vers:
                            vers = "Two versions defined"
                        else:
                            vers = "None"


                        command = subprocess.check_output("readelf -h {} | grep Magic".format(path), shell=True)
                        l = command.decode("utf-8").splitlines()
                        magic_number = l[0].replace(" ", "")



                        command = subprocess.check_output("readelf -h {} | grep Class".format(path), shell=True)
                        l = command.decode("utf-8").splitlines()
                        
                        if "32" in l[0]:
                            fclass = "32-bit"
                        elif "64" in l[0]:
                            fclass = "64-bit"
                        else:
                            fclass = "Unknown architecture"
                        
                    except ELFError:
                        status = False
                        machine_type = "NOT ELF"
                        magic_number = "NONE"
                        e_entry = "NONE"
            except IOError:
                status = False
                machine_type = "IO Error"
                magic_number = "IO Error"
            print("Elf Status: {}".format(status))
            print("Machine type: {}, {}".format( machine_type, fclass))
            print(magic_number)
            print("Object file type: {}".format(typef))
            print("File version: {}".format(vers))
            print("File entry point in hex: {}". format(hex(e_entry)))
            print("\n")

            return status, machine_type, magic_number, typef, e_entry
        else:
            print("No such directory: {}".format(path))
            sys.exit


    def dangerous_funcs(self):
        funcs=[]
        dstrings= []
        with open(path, 'rb') as t:
            out= subprocess.check_output("strings {}".format(path), shell=True)
            l = out.decode("utf-8").splitlines()
            if 'strcpy' in l: funcs.append("strcpy")
            if 'gets' in l: funcs.append("gets")
            if 'system' in l: funcs.append("system")
            if 'scanf' in l: funcs.append("scanf")
            if 'sprintf' in l: funcs.append("sprintf")
            if 'memcpy' in l: funcs.append("memcpy")
            
            for line in l:
                if re.search("http://", line, re.I): dstrings.append(line)
                if re.search("ftp://", line, re.I): dstrings.append(line)
                if re.search("pwd", line, re.I): dstrings.append(line)
                if re.search("pass", line, re.I): dstrings.append(line)
                if re.search("user", line, re.I): dstrings.append(line)
                if re.search("username", line, re.I): dstrings.append(line)
                if re.search("password", line, re.I): dstrings.append(line)
                if re.search("passwd", line, re.I): dstrings.append(line)
                if re.search("telnetd", line, re.I): dstrings.append(line)
                if re.search("ip address", line, re.I): dstrings.append(line)
                if re.search("server", line, re.I): dstrings.append(line)


            all_funcs = ", ".join(funcs)
            all_dstrings = ", ".join(dstrings)

            if len(all_funcs) > 0:
                print(self.name + "\033[91m"+all_funcs + "\033[39m")
            else:
                print("\033[92mNo dangerous function fuound \033[39m")
            if len(all_dstrings) > 0:
                print(self.dstrings + "\033[91m"+all_dstrings + "\033[39m")
            else:
                print ("\033[92mNo dangerous string found \033[39m")
            print("\n")
        return all_funcs, all_dstrings


    def get_packed(self):
        match = []
        with open(path, 'rb') as t:
            out= subprocess.check_output("strings {}".format(path), shell=True)
            l = out.decode("utf-8").splitlines()
            for line in l:
                if re.search("UPX", line, re.I): match.append(line)
                if re.search("Aspack", line, re.I): match.append(line)
                if re.search("adata", line, re.I): match.append(line)
                if re.search("NTKrnl", line, re.I): match.append(line)
                if re.search("PECompact", line, re.I): match.append(line)
                if re.search("PEC2", line, re.I): match.append(line)
                if re.search("Themida", line, re.I): match.append(line)
                if re.search("aPa2Wa", line, re.I): match.append(line)
        if len(match) > 0:
            all_matches = ", ".join(match)
            print("\033[91m " + all_matches + "\033[39m")
            print("\n")
        elif len(match) == 0:
            print("\033[92mNo packing detected \033[39m")
            print("\n")
        else:
            print("\033[91mErro with file\033[39m")
            print("\n")
        

    def reference_func(self):
        print("Checking functions referenced...")
        
        with open(path, 'rb') as f:
            e = pwnlib.elf.ELF(path, checksec=False)
            for section in e.iter_sections():
                if section.name.startswith('.text'):
                    tops = self.top_function(e, section.header['sh_addr'], section.header['sh_size'])
        s = "  %s\n"%(path.ljust(60))
        print(s)
        print("\n")
        for sym, n in tops:
            s = "  %s: ref %4d\n" %(sym.ljust(60), n)
            print(s)



    def top_function(self, elf, entry, size, top=10):
        calls = {}
        start = entry
        _size = min(size, 0x1000)
        

        while size > 0:
            data = elf.read(start, _size)
            
            try:
                lines = pwnlib.asm(data, arch=elf.arch, vma=start).split("\n")
            except TypeError:
                return sorted(calls.items(), key= lambda x:[1])[::-1][:top]

            start = start + len(data)
            size  = size - len(data)
            for i in range(len(lines)):
                s = re.sub(' +', ' ', lines[i])
                if "call" in s:
                    try:
                        addr = int(s.split(" ")[-1], 16)
                        sym =  hex(addr)
                        for _sym in elf.symbols:
                            if elf.symbols[_sym] == addr:
                                sym = _sym
                                break

                        if sym in calls:
                            calls[sym] = calls[sym] + 1
                        else:
                            calls[sym] = 1
                    except ValueError:
                        pass
        return sorted(calls.items(), key=lambda x: x[1])[::-1][:top]

    def dependency_check(self):
        print('Finding dependencies of {}...'.format(path))
        unique_deps = []
        deps = []
        with open(path, 'rb') as f:
            e = ELFFile(f)
            for section in e.iter_sections():
                if isinstance(section, DynamicSection):
                    for tag in section.iter_tags():
                        if tag['d_tag'] == 'DT_NEEDED':
                            deps.append(tag.needed)
                            if tag.needed not in unique_deps:
                                unique_deps.append(tag.needed)
        trunc = ','.join(deps[0:3])
        if len(deps) > 3:
            trunc += '...'

        if len(deps) > 1:
            s = " %s| %d dependencies found (%s)\n" %(path.ljust(30), len(deps), trunc)
            print(s)
        elif len(deps) == 1:
            s = " %s| %d dependency found (%s)\n" %(path.ljust(30), len(deps), trunc)
            print(s)
        else:
            print("Error tracing dependencies")

        for dep in deps:
            print(" %s\n"%(dep))

        if len(unique_deps) > 1:
            print("Found %d unique dependencies\n" %(len(unique_deps)))
        elif len(unique_deps) == 1:
            print("Found %d unique dependency\n" %(len(unique_deps)))
        else:
            print("Error tracing unique dependency")


    def check_sec(self):
        print("Checking security mitigations...")    
        with open(path, 'rb') as f:
            e = pwnlib.elf.ELF(path, checksec=False)
            f = e.checksec()
            x = Fortify(path)
            r = x.check_fortify()
            print(f)
            print(r)
            print("\n")


    def checksum(self):
        with open(path, 'rb') as f:
            md5 = hashlib.md5()
            sha1 = hashlib.sha1()
            sha256 = hashlib.sha256()
            sha512 = hashlib.sha512()
            md5 = hashlib.md5()

            while True:
                salt = f.read(16 * 1024)
                if not salt:
                    break
                md5.update(salt)
                sha1.update(salt)
                sha256.update(salt)
                sha512.update(salt)

            print("MD5: %s" % md5.hexdigest())
            print("SHA1: %s" % sha1.hexdigest())
            print("SHA256: %s" %sha256.hexdigest())
            print("SHA512: %s" % sha512.hexdigest())
            print("\n")


    def entropy(self):
        with open(path, 'rb') as f:
            byte_arrary = list(f.read())
        fileSize = len(byte_arrary)
        print('File size in bytes: {:,d}'.format(fileSize))

        freqList = []
        for b in range(256):
            counter = 0
            for byte in byte_arrary:
                if byte == b:
                    counter += 1
            freqList.append(float(counter) / fileSize)
        # Shannon entropy
        entr = 0.0
        for freq in freqList:
            if freq > 0:
                entr = entr + freq * math.log(freq, 2)
        entr = -entr
        if entr <= 7.7:
            print('Shannon entropy: '+'\033[92m {} \033[39m'.format(entr))
            print("\n")
        else:
            print('Shannon entropy:'+'\033[91m {} \033[39m'.format(entr))
            print("\n")



# path ='./libmodbus.so'
# analyzer = ELF_Functions(path)
# analyzer.main()
