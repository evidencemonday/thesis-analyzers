import re


_PEM_TO_CLASS = {
b"CERTIFICATE": 'Certificate',
b"PRIVATE KEY": 'PrivateKey',
b"PUBLIC KEY": 'PublicKey',
b"ENCRYPTED PRIVATE KEY": 'PrivateKey',
b"OPENSSH PRIVATE KEY": 'OpenSSHPrivateKey',
b"RSA PRIVATE KEY": 'RSAPrivateKey',
b"RSA PUBLIC KEY": 'RSAPublicKey',
b"EC PRIVATE KEY": 'ECPrivateKey',
b"DH PARAMETERS": 'DHParameters',
b"NEW CERTIFICATE REQUEST": 'CertificateRequest',
b"CERTIFICATE REQUEST": 'CertificateRequest',
b"X509 CRL": 'CertificateRevocationList',
}  # type: Dict[bytes, Type[AbstractPEMObject]]

_PEM_RE = re.compile(
    b"-----BEGIN ("
    + b"|".join(_PEM_TO_CLASS.keys())
    + b""")-----\r?
.+?\r?
-----END \\1-----\r?\n?""",
    re.DOTALL,
)


def pem_parser(pem_str):
    # type: (bytes) -> List[AbstractPEMObject]
    """
    Extract PEM objects from *pem_str*.
    :param pem_str: String to parse.
    :type pem_str: bytes
    :return: list of :ref:`pem-objects`
    """
    # _PEM_TO_CLASS[match.group(1)]
    for match in _PEM_RE.finditer(pem_str):
       return ( _PEM_TO_CLASS[match.group(1)])

def parse_file(file_path):
    # type: (str) -> List[AbstractPEMObject]
    """
    Read *path* and parse PEM objects from it using :func:`parse`.
    """
    with open(file_path, "rb") as f:
        return pem_parser(f.read())
