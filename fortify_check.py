from elftools.elf.elffile import ELFFile
from elftools.elf.elffile import *


class Fortify:

    def __init__(self, path):
        self.path = path


    def check_fortify(self):
        unique_deps = []
        deps = []
        with open(path, 'rb') as t:
            e = ELFFile(t)
            funcs = self.get_func_used(e)
            fortify = self.fortify(funcs)

        if fortify:
            print("\033[92mFortify:  \033[92mOn\033[39m")
        else:    
            print("Fortify:  \033[91mOff\033[39m")


    def fortify(self, funcs):
        for func in funcs:
            if func.endswith("_chk"):
                return True
            return False
        

    def get_func_used(self, e):
        jmp_slot =  []
        for section in e.iter_sections():
            if not isinstance(section, RelocationSection):
                continue
            symtable = e.get_section(section['sh_link'])
            for rel in section.iter_relocations():
                if isinstance(symtable, NullSection):
                    continue
                symbol = symtable.get_symbol(rel['r_info_sym'])
                if rel['r_info_type'] == 7:
                    jmp_slot.append(symbol.name)

        return jmp_slot


path='./test_files/libmodbus.so'
analyzer = Fortify(path)
analyzer.check_fortify()
