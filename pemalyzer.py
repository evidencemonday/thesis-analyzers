import os
import sys
import pem
import re
import hashlib
from OpenSSL import crypto
import sys
import random
import math
import subprocess, gmpy2, configparser
from gmpy2 import mpz
from file_validator import parse_file as ps


class Pemalyzer:
    def __init__(self, path):
        self.path = path

    def X509(self):
        cert = crypto.load_certificate(crypto.FILETYPE_PEM, open(path).read())
        public_key= cert.get_pubkey()
        key_size = public_key.bits()
        expired = cert.has_expired()
        sign_algorithm = cert.get_signature_algorithm()
        sign_alg = sign_algorithm.decode("utf-8")
        converted_alg = sign_alg.replace(" ", "")

        def __key_size__(): 
            if key_size < 1024:
	            print("\033[91m%i bits, that's very short and insecure, use 2048 bit or more\033[39m" % key_size)
            
            elif key_size < 2048:
	            print("\033[93m%i bits, better use 2048 bit or more\033[39m" % key_size)
            else:
	            print("\033[92m%i bits, that should be long enough\033[39m" % key_size)

            #return key_size

        def expiry_date():  
            if expired == False:
                print("\033[92m%s, Certificate is still valid\033[39m" % expired)
            elif expired == True:
                print("\033[91m%s, Ceritificate has expired.\033[39m" % expired)
            else:
                print("\033[91m%s is not a valid certificate file\033[39m" % path)
            #return expired

        def algorithm():
            if 'sha2'in sign_algorithm.decode():
                print("\033[92m Strong Encryption algorithm: %s \033[39m" % converted_alg)
            else:
                print("\033[91mWeak encryption algorithm used: %s, Use SHA2 instead \033[39m" % converted_alg)
        return algorithm(), expiry_date(), __key_size__()

    def rsa(self):
        out = subprocess.check_output("openssl rsa -in {} -text -noout".format(path), shell=True)
        l = out.decode("utf-8").splitlines()

        def get_first_prime(path):
            prime_string = ""
            string_key = ""

            for line in l:
                if "prime1" in line:
                    line_index=l.index(line) +1
                    line_list = l[line_index:]

                    for lin in line_list:
                        if "prime2" in lin:
                            line2_index=l.index(lin)
                            line2_list = l[line2_index:]
                            last_list = l[line_index:line2_index]

                            for i in last_list:
                                y = i.replace(" ", "")
                                e = y.replace(":", "")
                                string_key = string_key + e
                        else:
                            pass
            prime = mpz(string_key, 16)
            prime_string = hex(prime)
            return prime
        
        def get_second_prime(path):
            prime_string = ""
            string_key = ""

            for line in l:
                if "prime2" in line:
                    line_index=l.index(line) +1
                    line_list = l[line_index:]

                    for lin in line_list:
                        if "exponent1" in lin:
                            line2_index=l.index(lin)
                            line2_list = l[line2_index:]
                            last_list = l[line_index:line2_index]

                            for i in last_list:
                                y = i.replace(" ", "")
                                e = y.replace(":", "")
                                string_key = string_key + e
                        else:
                            pass
            prime = mpz(string_key, 16)
            prime_string = hex(prime)
            return prime


        def run(path):
            '''
            Since RSA Private keys have two primes
            '''
            prime = get_first_prime(path) + get_second_prime(path)
            bit1 = get_second_prime(path).bit_length()
            bit2 = get_first_prime(path).bit_length()
            bits = bit1 + bit2

            print("checking prime p = %s" % hex(prime))

            # check for known primes
            config = configparser.ConfigParser()
            config.read(os.path.dirname(sys.argv[0])+'/knownprimes.ini')

            for sect in config.sections():
	            if config.get(sect, 'prime') == prime:
		            print("This is known prime %s" % sect)

            if bits < 1024:
	            print("\033[91m%i bits, that's very short and insecure, use 2048 bit or more\033[39m" % bits)
            elif bits < 2048:
	            print("\033[93m%i bits, better use 2048 bit or more\033[39m" % bits)
            else:
	            print("\033[92m%i bits, that should be long enough\033[39m" % bits)

            if gmpy2.is_prime(prime):
	            print("\033[92mp is prime\033[39m")
            else:
	            print("\033[91mp is not a prime or may be broken\033[39m")

            p12 = gmpy2.add(gmpy2.mul(prime, 2), 1)

            if gmpy2.is_prime(p12):
	            print("\033[92m2p+1 is a Sophie Germain Prime, p is a safe prime\033[39m")
            else:
	            print("\033[93m2p+1 is not a prime, therefore p is no safe prime\033[39m")
        return run(path)

    def diffie(self):

        def key_length():
            out= subprocess.check_output("openssl pkeyparam -in {} -text".format(path), shell=True)
            l = out.decode("utf-8").splitlines()
            prime_string = ""
            string_key = ""
            for line in l:
                if "prime" in line:
                    # Get the index of the next line containing 'prime'
                    line_index=l.index(line) +1
                    # Avoid the last line conataining generator: 0x2
                    line_list = l[line_index:-1]
            
                    # Replace whitespace and : from index with 'prime' downwards with null
                    for i in line_list:
                        y = i.replace(" ", "")
                        e = y.replace(":", "")
                        string_key = string_key + e
                else:
                    pass

            # Get prime
            prime = mpz(string_key, 16)
            prime_string = hex(prime)
            print("checking prime p = %s" % prime_string)
            bits = prime.bit_length()


            config = configparser.ConfigParser()
            config.read(os.path.dirname(sys.argv[0])+'/knownprimes.ini')

            for sect in config.sections():
	            if config.get(sect, 'prime') == prime_string:
		            print("This is known prime %s" % sect)

            if bits < 1024:
	            print("\033[91m%i bits, that's very short and insecure, use 2048 bit or more\033[39m" % bits)
            elif bits < 2048:
	            print("\033[93m%i bits, better use 2048 bit or more\033[39m" % bits)
            else:
	            print("\033[92m%i bits, that should be long enough\033[39m" % bits)

            if gmpy2.is_prime(prime):
	            print("\033[92mp is prime\033[39m")
            else:
	            print("\033[91mnoprimep is not a prime, that is broken\033[39m")

            p12 = gmpy2.add(gmpy2.mul(prime, 2), 1)

            if gmpy2.is_prime(p12):
	            print("\033[92m2p+1 is a Sophie Germain Prime, p is a safe prime\033[39m")
            else:
	            print("\033[93m2p+1 is not a prime, therefore p is no safe prime\033[39m")

        return key_length()

    def csr(self):
        def check_modulus(path):
            out = subprocess.check_output("openssl req -in {} -text -noout -subject -pubkey".format(path), shell=True)
            l = out.decode("utf-8").splitlines()

            prime_string = ""
            string_key = ""

            for line in l:
                if "Modulus" in line:
                    line_index=l.index(line) +1
                    line_list = l[line_index:]

                    for lin in line_list:
                        if "Exponent" in lin:
                            line2_index=l.index(lin)
                            line2_list = l[line2_index:]
                            last_list = l[line_index:line2_index]

                            for i in last_list:
                                y = i.replace(" ", "")
                                e = y.replace(":", "")
                                string_key = string_key + e
                        else:
                            pass
            prime = mpz(string_key, 16)
            prime_string = hex(prime)
            return prime

        def run():
    
            prime = check_modulus(path)
            bits = prime.bit_length()

            print("checking prime p = %s" % hex(prime))

            # check for known primes
            config = configparser.ConfigParser()
            config.read(os.path.dirname(sys.argv[0])+'/knownprimes.ini')

            for sect in config.sections():
	            if config.get(sect, 'prime') == prime:
		            print("This is known prime %s" % sect)

            if bits < 1024:
	            print("\033[91m%i bits, that's very short and insecure, use 2048 bit or more\033[39m" % bits)
            elif bits < 2048:
	            print("\033[93m%i bits, better use 2048 bit or more\033[39m" % bits)
            else:
	            print("\033[92m%i bits, that should be long enough\033[39m" % bits)

            if gmpy2.is_prime(prime):
	            print("\033[92mp is prime\033[39m")
            else:
	            print("\033[91mp is not a prime or may be broken\033[39m")

            p12 = gmpy2.add(gmpy2.mul(prime, 2), 1)

            if gmpy2.is_prime(p12):
	            print("\033[92m2p+1 is a Sophie Germain Prime, p is a safe prime\033[39m")
            else:
	            print("\033[93m2p+1 is not a prime, therefore p is no safe prime\033[39m")

            out = subprocess.check_output("openssl req -noout -text -in {} | grep 'Signature Algorithm'".format(path), shell=True)
            sign_algorithm = out.decode("utf-8")
            converted_alg = sign_algorithm.replace(" ", "")

            if 'sha2'in sign_algorithm:
                print("\033[92m%s Strong Encryption algorithm.\033[39m" % converted_alg)
            elif 'md5' in sign_algorithm:
                print("\033[91mWeak algorithm used: MD5. Use SHA2 instead \033[39m")
            elif 'sha1' in sign_algorithm:
                print("\033[93mSHA1: Still ok but SHA2 is preferred\033[39m")
            else:
                print("\033[91m%sWeak algorithm used. Use SHA2 instead \033[39m" % converted_alg)

        return run()

    
    def check_cert_type(self):
        certificateType = ps(path)
        print(certificateType)

        if certificateType == "Certificate":
            Pemalyzer.X509(self)
        elif certificateType == "RSAPrivateKey":
            Pemalyzer.rsa(self)
        elif certificateType == "DHParameters":
            Pemalyzer.diffie(self)
        elif certificateType == "CertificateRequest":
            Pemalyzer.csr(self)
        else:
            print("Unrecognized certificate type")

    def split_cert(self):
        print("Splitting chain certificate...")
        if os.path.exists("./tmp"): 
            os.system("rm -r ./tmp")
        tmp = os.mkdir("./tmp")
        os.system("./split.sh {}".format(path))

        with os.scandir("./tmp") as entries:
            for entry in entries:
                Pemalyzer.check_cert_type(self)

    def main(self):
        if len(pem.parse_file(path)) == 1:
            Pemalyzer.check_cert_type(self)
        elif len(pem.parse_file(path)) > 1:
            Pemalyzer.split_cert(self)
        elif len(pem.parse_file(path)) == 0:
            print("Empty certificate file")
        else:
            print("Wrong certificate file")


path ='./routers/netgear/netgear_R6220/client.csr'
#path = "./Phones_Video/polycom/polycom-hdx-release-3.1.14-56008/SeqCert.pem"
# path = "Phones_Video/polycom/polycom-hdx-release-3.1.14-56008/csapi.pem"

analyzer = Pemalyzer(path)
analyzer.main()
